<?php

if (!empty($argv[1])) {
    $_GET = parse_argv($argv);
}
$read_file      = isset($_GET['file']) ? $_GET['file'] : '';
$output_file    = isset($_GET['unique-combinations']) ? $_GET['unique-combinations'] : time().'.csv';

if ($read_file == '' || $output_file == '') {
    print_r('file name not found');
    die;
}

$file_exists = file_exists($read_file);
if ($file_exists != '1') {
    print_r($read_file . ' file does not exits');
    die;
}

$require_field  = isset($_GET['required-fields']) ? explode(',', $_GET['required-fields']) : array();
$row            = 1;
$array          = array();
$extension      = pathinfo($read_file, PATHINFO_EXTENSION);

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);

if (($handle = fopen($read_file, "r")) !== FALSE) {
    $columns        = array();
    $condition_array= array();
    $file_data      = array();

    if ($extension == 'csv') {
        $columns = fgetcsv($handle, 1000, ",");
        $columns = array_filter($columns);
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $temp_data = array();
            $all_blank = true;
            for ($j= 0; $j < count($columns); $j++) {
                $temp_data[$j] = $data[$j];
                if ($data[$j] != '') {
                    $all_blank = false;
                }
            }
            if (! $all_blank) {
                $file_data[] = $temp_data;
            }
        }
    } else if ($extension == 'json' || $extension == 'xml') {
        $row_data = array();
        if ($extension == 'json') {
            $json       = file_get_contents($read_file);
            $row_data  = json_decode($json,true);
        } else if ($extension == 'xml') {
            $xml_data   = simplexml_load_file($read_file);
            $xml_row    = json_decode(json_encode($xml_data), TRUE);
            if (isset($xml_row['row']) && ! empty($xml_row['row'])) {
                $row_data = $xml_row['row'];
            }
        }

        foreach($row_data as $row_record) {
            $temp_array = array();
            $all_blank  = true;
            foreach($row_record as $j_key => $j_value) {
                if (strpos($j_key, 'FIELD') === FALSE && (! is_array($j_value))) {
                    $columns[]      = $j_key;
                    $temp_array[]   = $j_value;

                    if ($j_value != '') {
                        $all_blank = false;
                    }
                }
            }

            if (! $all_blank) {
                $file_data[] = $temp_array;
            }
        }

        $columns = array_unique($columns);
    } else {
        print_r('File formate not supported, please use XML, CSV or JSON file');
        die;
    }

    foreach ($columns as $key => $v) {
        if (in_array($v, $require_field)) {
            $condition_array[$key] = $key;
        }
    }

    $columns        = array_filter($columns);
    if (empty($columns)) {
        print_r('No column found(s)');
        die;
    }

    $column_name    = $columns;
    $column_name[]  = 'Count';
    if (count(array_intersect($columns, $require_field)) != count($require_field)) {
        print_r('Required column(s) not found');
        die;
    }

    if (empty($file_data)) {
        print_r('No data found(s)');
        die;
    }
    
    foreach($file_data as $row) {
        $null_value = array_keys($row, null);
        if (empty(array_intersect($null_value, $condition_array))) {
            $value_array = array();
            for ($j = 0; $j < count($columns); $j++) {
                $value_array[$columns[$j]] = $row[$j];
            }
            $array[] = $value_array;
        }
    }

    $hash       = array();
    $array_out  = array();

    foreach ($array as $item) {
        $item['count']  = 0;
        $hash_key       = implode('|', $item);

        if (!array_key_exists($hash_key, $hash)) {
            $hash[$hash_key] = sizeof($array_out);
            array_push($array_out, $item);
        }
        $array_out[$hash[$hash_key]]['count'] += 1;
    }
    fclose($handle);
    $file_exist_output = file_exists($output_file);
    if ($file_exist_output == '1') {
        unlink($output_file);
    }

    $f = fopen($output_file, 'a');
    fputcsv($f, $column_name);

    foreach ($array_out as $val) {
        fputcsv($f, $val);
    }

    fclose($f);
    print_r($output_file.' file created with count');
    die();
}

function parse_argv(array $argv): array {
    $request = [];
    foreach ($argv as $i => $a) {
        if (!$i) {
            continue;
        }

        if (preg_match('/^-*(.+?)=(.+)$/', $a, $matches)) {
            $request[$matches[1]] = $matches[2];
        } else {
            $request[$a] = true;
        }
    }

    return $request;
}
?>
