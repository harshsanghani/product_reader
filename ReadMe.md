# Sample command to execute file :

php parser.php --file=product_small.csv --unique-combinations=combination_count.csv --required-fields=brand_name,model_name

### Parameters :

1) --file 					
    - This will be the input file, You can change the name accordingly - REQUIRED
    - CSV, XML & JSON file supported

2) --unique-combinations	
    - This will be the output file name	- OPTIONAL

3) --required-fields		
    - You can define the column name which is must required - OPTIONAL

### Validation :

	- Input file must be located at same directory from where you execute the file
	- Required columns If defined then must be available in file

### Result :

OUTPUT_FILENAME file created with count